package org.luisma.lugarfinder.adapter;


import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import org.luisma.lugarfinder.R;
import org.luisma.lugarfinder.database.Database;
import org.luisma.lugarfinder.lugares.Lugar;

import java.util.ArrayList;

public class LugaresAdapter extends BaseAdapter{

    private ArrayList<Lugar> lugares;
    private LayoutInflater inflador;
    private Context context;
    public LugaresAdapter(Context context, ArrayList<Lugar> lugares){
        this.context = context;
        this.lugares = lugares;
        inflador = LayoutInflater.from(context);
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        LugaresHolder lugaresHolder = null;

        if (view == null){
            lugaresHolder = new LugaresHolder();

            view = inflador.inflate(R.layout.fila_lugares ,null);
            lugaresHolder.panel = (LinearLayout) view.findViewById(R.id.panel);

            lugaresHolder.imagenLugar = (ImageView) view.findViewById(R.id.imagen_lugar);
            lugaresHolder.nombreLugar = (TextView) view.findViewById(R.id.nombre_lugar);
            lugaresHolder.tipoLugar = (TextView) view.findViewById(R.id.tipo_lugar);
            lugaresHolder.descripcionLugar = (TextView) view.findViewById(R.id.descripcion_lugar);
            lugaresHolder.direccionLugar = (TextView) view.findViewById(R.id.direccion_lugar);
            lugaresHolder.opinionLugar = (TextView) view.findViewById(R.id.opinion_lugar);

            view.setTag(lugaresHolder);
        }else{
            lugaresHolder = (LugaresHolder) view.getTag();
        }

        Lugar lugar = lugares.get(i);
        if (lugar.getImagen() != null)
            lugaresHolder.imagenLugar.setImageBitmap(lugar.getImagen());
        lugaresHolder.nombreLugar.setText(lugar.getNombre());
        lugaresHolder.tipoLugar.setText(lugar.getTipo());
        lugaresHolder.descripcionLugar.setText(lugar.getDescripcion());
        lugaresHolder.direccionLugar.setText(lugar.getDireccion());
        if ((Integer)lugar.getOpinion() != null)
            lugaresHolder.opinionLugar.setText(String.valueOf(lugar.getOpinion()));
        else
            lugaresHolder.opinionLugar.setText(R.string.sinopinion);

        SharedPreferences preferencias = PreferenceManager.getDefaultSharedPreferences(context);
        int color = preferencias.getInt("color", 000000);
        lugaresHolder.panel.setBackgroundColor(color);

        return view;
    }

    static class LugaresHolder {
        LinearLayout panel;
        ImageView imagenLugar;
        TextView nombreLugar;
        TextView tipoLugar;
        TextView descripcionLugar;
        TextView direccionLugar;
        TextView opinionLugar;
    }

    @Override
    public int getCount() {
        return lugares.size();
    }

    @Override
    public Object getItem(int i) {
        return lugares.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

}
