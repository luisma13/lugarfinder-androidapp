package org.luisma.lugarfinder.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import org.luisma.lugarfinder.R;
import org.luisma.lugarfinder.lugares.Alojamiento;
import org.luisma.lugarfinder.lugares.Restaurante;

import java.util.ArrayList;
import java.util.Random;

public class AlojamientosAdapter extends BaseAdapter {

    private ArrayList<Alojamiento> alojamientos;
    private LayoutInflater inflador;
    private Context context;

    public AlojamientosAdapter(Context context, ArrayList<Alojamiento> alojamientos){
        this.context = context;
        this.alojamientos = alojamientos;
        inflador = LayoutInflater.from(context);
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        AlojamientosHolder alojamientosHolder = null;

        if (view == null){
            alojamientosHolder = new AlojamientosHolder();

            view = inflador.inflate(R.layout.fila_alojamiento ,null);
            alojamientosHolder.panelAlojamiento = (LinearLayout) view.findViewById(R.id.panelAlojamiento);
            alojamientosHolder.imagenAlojamiento = (ImageView) view.findViewById(R.id.imagen_alojamiento);
            alojamientosHolder.nombreAlojamiento = (TextView) view.findViewById(R.id.nombre_alojamiento);
            alojamientosHolder.direccionAlojamiento = (TextView) view.findViewById(R.id.direccion_alojamiento);
            alojamientosHolder.categoriaAlojamiento = (TextView) view.findViewById(R.id.categoria_alojamiento);

            view.setTag(alojamientosHolder);
        }else{
            alojamientosHolder = (AlojamientosHolder) view.getTag();
        }
        Alojamiento alojamiento = alojamientos.get(i);
        Random rnd = new Random();
        int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
        alojamientosHolder.imagenAlojamiento.setImageResource(R.drawable.icono_alojamiento);
        alojamientosHolder.imagenAlojamiento.setColorFilter(color);

        alojamientosHolder.nombreAlojamiento.setText(alojamiento.getNombre());
        alojamientosHolder.direccionAlojamiento.setText(alojamiento.getDireccion());
        alojamientosHolder.categoriaAlojamiento.setText(alojamiento.getCategoria());

        SharedPreferences preferencias = PreferenceManager.getDefaultSharedPreferences(context);
        int color2 = preferencias.getInt("color", 000000);
        alojamientosHolder.panelAlojamiento.setBackgroundColor(color2);

        return view;
    }

    static class AlojamientosHolder {
        LinearLayout panelAlojamiento;
        ImageView imagenAlojamiento;
        TextView nombreAlojamiento;
        TextView direccionAlojamiento;
        TextView categoriaAlojamiento;
    }

    @Override
    public int getCount() {
        return alojamientos.size();
    }

    @Override
    public Object getItem(int i) {
        return alojamientos.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

}
