package org.luisma.lugarfinder.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import org.luisma.lugarfinder.R;
import org.luisma.lugarfinder.lugares.Restaurante;

import java.util.ArrayList;
import java.util.Random;

public class RestauranteAdapter extends BaseAdapter {

    private ArrayList<Restaurante> restaurantes;
    private LayoutInflater inflador;
    private Context context;
    public RestauranteAdapter(Context context, ArrayList<Restaurante> restaurantes){
        this.context = context;
        this.restaurantes = restaurantes;
        inflador = LayoutInflater.from(context);
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        RestauranteHolder restauranteHolder = null;

        if (view == null){
            restauranteHolder = new RestauranteHolder();

            view = inflador.inflate(R.layout.fila_restaurante ,null);
            restauranteHolder.panelRestaurante = (LinearLayout) view.findViewById(R.id.panelRestaurante);
            restauranteHolder.imagenRestaurante = (ImageView) view.findViewById(R.id.imagen_restaurante);
            restauranteHolder.nombreRestaurante = (TextView) view.findViewById(R.id.nombre_restaurante);
            restauranteHolder.direccionRestaurante = (TextView) view.findViewById(R.id.direccion_restaurante);
            restauranteHolder.categoriaRestaurante = (TextView) view.findViewById(R.id.categoria_restaurante);

            view.setTag(restauranteHolder);
        }else{
            restauranteHolder = (RestauranteHolder) view.getTag();
        }

        Restaurante restaurante = restaurantes.get(i);
        Random rnd = new Random();
        int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
        restauranteHolder.imagenRestaurante.setImageResource(R.drawable.icono_plato);
        restauranteHolder.imagenRestaurante.setColorFilter(color);
        restauranteHolder.nombreRestaurante.setText(restaurante.getNombre());
        restauranteHolder.direccionRestaurante.setText(restaurante.getDireccion());
        restauranteHolder.categoriaRestaurante.setText(restaurante.getCategoria());

        SharedPreferences preferencias = PreferenceManager.getDefaultSharedPreferences(context);
        int color2 = preferencias.getInt("color", 000000);
        restauranteHolder.panelRestaurante.setBackgroundColor(color2);

        return view;
    }

    static class RestauranteHolder {
        LinearLayout panelRestaurante;
        ImageView imagenRestaurante;
        TextView nombreRestaurante;
        TextView direccionRestaurante;
        TextView categoriaRestaurante;
    }

    @Override
    public int getCount() {
        return restaurantes.size();
    }

    @Override
    public Object getItem(int i) {
        return restaurantes.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }


}
