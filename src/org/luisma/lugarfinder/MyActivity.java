package org.luisma.lugarfinder;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import org.luisma.lugarfinder.database.Database;
import org.luisma.lugarfinder.lugares.Alojamiento;
import org.luisma.lugarfinder.lugares.Restaurante;
import org.luisma.lugarfinder.util.AlojamientosFragment;

import java.util.ArrayList;

public class MyActivity extends Activity {

    public static String URLALOJAMIENTOS = "http://www.zaragoza.es/georref/json/hilo/ver_Alojamiento";
    public static String URLRESTAURANTES = "http://www.zaragoza.es/georref/json/hilo/ver_Restaurante";

    public static ArrayList<Restaurante> RESTAURANTES = new ArrayList<Restaurante>();
    public static ArrayList<Restaurante> RESTAURANTESTOTALES = new ArrayList<Restaurante>();
    public static ArrayList<Alojamiento> ALOJAMIENTOS = new ArrayList<Alojamiento>();
    public static Database db;

    private Fragment tabFragment1;
    private Fragment tabFragment2;
    private Fragment tabFragment3;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        db = new Database(this);
        loadTabs();
    }

    public void loadTabs(){
        ActionBar actionBar = getActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

        ActionBar.Tab tab1 = actionBar.newTab().setText(R.string.misLugares);
        ActionBar.Tab tab2 = actionBar.newTab().setText(R.string.restaurantes);
        ActionBar.Tab tab3 = actionBar.newTab().setText(R.string.alojamientos);

        tabFragment1 = new LugaresFragment();
        tabFragment2 = new RestaurantesFragment();
        tabFragment3 = new AlojamientosFragment();

        cargarDatos((RestaurantesFragment)tabFragment2, (AlojamientosFragment)tabFragment3);

        tab1.setTabListener(new TabsListener(tabFragment1));
        tab2.setTabListener(new TabsListener(tabFragment2));
        tab3.setTabListener(new TabsListener(tabFragment3));

        actionBar.addTab(tab1);
        actionBar.addTab(tab2);
        actionBar.addTab(tab3);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.optionmenu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()){
            case R.id.opt_nuevo_lugar:
                intent = new Intent(this, NuevoLugarActivity.class);

                Bundle bundle = new Bundle();
                bundle.putString("accion", "nuevo");
                intent.putExtras(bundle);
                this.startActivity(intent);
                return true;
            case R.id.opt_preferencias:
                intent = new Intent(this, Preferencias.class);
                this.startActivity(intent);
                return true;
            case R.id.opt_acercade:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage(R.string.mensajeacercade)
                        .setIcon(R.drawable.acercade)
                        .setTitle(R.string.acercade)
                        .setPositiveButton(R.string.aceptar, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                AlertDialog dialogo = builder.create();
                dialogo.show();
            case R.id.opt_restaurantes_cercanos:
                intent = new Intent(this, Mapa.class);

                Bundle bundle2 = new Bundle();
                intent.putExtra("todos", true);
                intent.putExtras(bundle2);
                this.startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void cargarDatos(RestaurantesFragment tabFragment2, AlojamientosFragment tabFragment3){
        tabFragment2.CARGARDATOS();
        tabFragment3.CARGARDATOS();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}
