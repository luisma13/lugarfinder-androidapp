package org.luisma.lugarfinder;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.luisma.lugarfinder.adapter.RestauranteAdapter;
import org.luisma.lugarfinder.lugares.Restaurante;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class RestaurantesFragment extends Fragment implements OnItemClickListener {

    private RestauranteAdapter restauranteAdapter;
    private ListView listViewRestaurantes;
    private boolean toprestaurantes;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.restaurantes, container, false);
        SharedPreferences preferencias = PreferenceManager.getDefaultSharedPreferences(view.getContext());
        toprestaurantes = preferencias.getBoolean("restaurantesfiltro", false);
        boolean descendente = preferencias.getBoolean("restaurantes_descendentes", false);
        if (toprestaurantes) {
            filtrarRestaurantes();
            if(descendente)
                ordenDescendente();
        }
        else{
            if(descendente)
                ordenDescendente();
            else {
                MyActivity.RESTAURANTES.clear();
                MyActivity.RESTAURANTES.addAll(MyActivity.RESTAURANTESTOTALES);
            }
        }
        listViewRestaurantes = (ListView) view.findViewById(R.id.lista_Restaurantes);

        restauranteAdapter = new RestauranteAdapter(getActivity(), MyActivity.RESTAURANTES);
        listViewRestaurantes.setAdapter(restauranteAdapter);
        listViewRestaurantes.setOnItemClickListener(this);
        restauranteAdapter.notifyDataSetChanged();
        return view;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        if (i == ListView.INVALID_POSITION)
            return;

        Restaurante restaurante = MyActivity.RESTAURANTES.get(i);

        Intent intent = new Intent(getActivity(), Mapa.class);
        intent.putExtra("latitud", restaurante.getLatitud());
        intent.putExtra("longitud", restaurante.getLongitud());
        intent.putExtra("nombre", restaurante.getNombre());
        intent.putExtra("todos", false);
        startActivity(intent);
    }

    public void CARGARDATOS(){
        TareaDescargaDatosRestaurantes descargaDatosRestaurantes = new TareaDescargaDatosRestaurantes();
        descargaDatosRestaurantes.execute(MyActivity.URLRESTAURANTES);
    }

    public void ordenDescendente(){
        ArrayList<Restaurante> ordenDescendente = new ArrayList<Restaurante>();
        for (int i = MyActivity.RESTAURANTES.size()-1; i >=0 ; i--){
            ordenDescendente.add(MyActivity.RESTAURANTES.get(i));
        }
        MyActivity.RESTAURANTES.clear();
        MyActivity.RESTAURANTES.addAll(ordenDescendente);
        if (restauranteAdapter != null)
            restauranteAdapter.notifyDataSetChanged();
    }

    public class TareaDescargaDatosRestaurantes extends AsyncTask<String, Void, Void> {

        private boolean error = false;

        @Override
        protected Void doInBackground(String... urls) {

            InputStream is = null;
            String resultado = null;
            JSONObject json = null;
            JSONArray jsonArray = null;
            MyActivity.RESTAURANTES.clear();
            MyActivity.RESTAURANTESTOTALES.clear();
            try {
                HttpClient clienteHttp = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost(urls[0]);
                HttpResponse respuesta = clienteHttp.execute(httpPost);
                HttpEntity entity = respuesta.getEntity();
                is = entity.getContent();

                BufferedReader br = new BufferedReader(new InputStreamReader(is));
                StringBuilder sb = new StringBuilder();
                String linea = null;

                while ((linea = br.readLine()) != null)
                    sb.append(linea + "\n");

                is.close();
                resultado = sb.toString();

                json = new JSONObject(resultado);
                jsonArray = json.getJSONArray("features");

                String nombre = null;
                String direccion = null;
                String categoria = null;
                String coordenadas = null;
                Restaurante restaurante = null;
                for (int i = 0; i < jsonArray.length(); i++) {

                    nombre = jsonArray.getJSONObject(i).getJSONObject("properties").getString("title");
                    direccion = jsonArray.getJSONObject(i).getJSONObject("properties").getString("description");
                    categoria = jsonArray.getJSONObject(i).getJSONObject("properties").getString("category");
                    coordenadas = jsonArray.getJSONObject(i).getJSONObject("geometry").getString("coordinates");
                    coordenadas = coordenadas.substring(1, coordenadas.length() - 1);
                    String latlong[] = coordenadas.split(",");
                    restaurante = new Restaurante(nombre, direccion, categoria,Float.parseFloat(latlong[0]),
                            Float.parseFloat(latlong[1]));

                    MyActivity.RESTAURANTES.add(restaurante);
                    MyActivity.RESTAURANTESTOTALES.add(restaurante);
                }

            } catch (ClientProtocolException cpe) {
                cpe.printStackTrace();
                error = true;
            } catch (IOException ioe) {
                ioe.printStackTrace();
                error = true;
            } catch (JSONException jse) {
                jse.printStackTrace();
                error = true;
            }

            return null;
        }


        @Override
        protected void onCancelled() {
            super.onCancelled();
            MyActivity.RESTAURANTES.clear();
        }

        @Override
        protected void onProgressUpdate(Void... progreso) {
            super.onProgressUpdate(progreso);
        }

        @Override
        protected void onPostExecute(Void resultado) {
            super.onPostExecute(resultado);
            if (error) {
                Toast.makeText(getActivity(), getResources().getString(R.string.error), Toast.LENGTH_SHORT).show();
                return;
            }
            if(isAdded()){
                Toast.makeText(getActivity(), getResources().getString(R.string.datosdescargados), Toast.LENGTH_SHORT).show();
            }
            if (restauranteAdapter != null)
                restauranteAdapter.notifyDataSetChanged();
        }
    }

    public void filtrarRestaurantes(){
        for (int i = 0; i < MyActivity.RESTAURANTES.size(); i++){
            Restaurante restaurante = MyActivity.RESTAURANTES.get(i);
            if (restaurante.getCategoria().equalsIgnoreCase("1 Tenedor") ) {
                MyActivity.RESTAURANTES.remove(restaurante);
                i--;
            }
        }
        if (restauranteAdapter != null)
            restauranteAdapter.notifyDataSetChanged();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onResume() {
        super.onResume();
        restauranteAdapter.notifyDataSetChanged();
        SharedPreferences preferencias = PreferenceManager.getDefaultSharedPreferences(getActivity());
        toprestaurantes = preferencias.getBoolean("restaurantesfiltro", false);
        boolean descendente = preferencias.getBoolean("restaurantes_descendentes", false);
        if (toprestaurantes) {
            filtrarRestaurantes();
            if(descendente)
                ordenDescendente();
        }
        else{
            if(descendente)
                ordenDescendente();
            else {
                MyActivity.RESTAURANTES.clear();
                MyActivity.RESTAURANTES.addAll(MyActivity.RESTAURANTESTOTALES);
            }
        }
    }
}