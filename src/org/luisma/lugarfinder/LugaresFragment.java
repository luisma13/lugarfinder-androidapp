package org.luisma.lugarfinder;

import android.app.Fragment;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.*;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.ListView;
import android.widget.Toast;
import org.luisma.lugarfinder.adapter.LugaresAdapter;
import org.luisma.lugarfinder.database.Database;
import org.luisma.lugarfinder.lugares.Lugar;

import java.util.ArrayList;

public class LugaresFragment extends Fragment implements View.OnCreateContextMenuListener {

    public static ArrayList<Lugar> lugares = new ArrayList<Lugar>();
    private LugaresAdapter lugaresAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.lugares, container, false);
        lugares.clear();
        ListView listViewLugares = (ListView) view.findViewById(R.id.listaMisLugares);


        Cursor cursor = MyActivity.db.getLugares();

        for(cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
            Lugar lugar = new Lugar();
            lugar.setNombre(cursor.getString(1));
            lugar.setTipo(cursor.getString(2));
            lugar.setDescripcion(cursor.getString(3));
            lugar.setDireccion(cursor.getString(4));
            lugar.setOpinion(cursor.getInt(5));
            byte[] imageByte = cursor.getBlob(6);
            Bitmap image = BitmapFactory.decodeByteArray(imageByte, 0, imageByte.length);
            lugar.setImagen(image);
            lugares.add(lugar);
        }

        lugaresAdapter = new LugaresAdapter(getActivity(), lugares);
        listViewLugares.setAdapter(lugaresAdapter);
        listViewLugares.setOnCreateContextMenuListener(this);

        setHasOptionsMenu(true);

        return view;
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        getActivity().getMenuInflater().inflate(R.menu.contextmenulugar, menu);
    }


    @Override
    public boolean onContextItemSelected(MenuItem item) {

        AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();

        switch (item.getItemId()){

            case R.id.ctx_eliminar_lugar:
                eliminarLugar(info);
                return true;
            case R.id.ctx_editar_lugar:
                Intent editar = new Intent(getActivity(), NuevoLugarActivity.class);

                Bundle bundle = new Bundle();
                bundle.putString("accion", "editar");
                bundle.putInt("posicion", info.position);
                editar.putExtras(bundle);

                startActivity(editar);
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }


    public void eliminarLugar(AdapterContextMenuInfo info){
        Lugar lugar = lugares.get(info.position);
        int id = MyActivity.db.conocerIDLugar(lugar);
        MyActivity.db.eliminarLugar(id);
        lugares.remove(info.position);
        lugaresAdapter.notifyDataSetChanged();
    }

    @Override
    public void onResume() {
        super.onResume();
        lugares.clear();
        Database dbNoNull = null;
        Cursor  cursor = null;

        if (MyActivity.db == null) {
            dbNoNull = new Database(null);
            cursor = dbNoNull.getLugares();
        }
        else
            cursor = MyActivity.db.getLugares();

        for(cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
            Lugar lugar = new Lugar();
            lugar.setNombre(cursor.getString(1));
            lugar.setTipo(cursor.getString(2));
            lugar.setDescripcion(cursor.getString(3));
            lugar.setDireccion(cursor.getString(4));
            lugar.setOpinion(cursor.getInt(5));
            byte[] imageByte = cursor.getBlob(6);
            Bitmap image = BitmapFactory.decodeByteArray(imageByte, 0, imageByte.length);
            lugar.setImagen(image);
            lugares.add(lugar);
        }
        lugaresAdapter.notifyDataSetChanged();
    }
}