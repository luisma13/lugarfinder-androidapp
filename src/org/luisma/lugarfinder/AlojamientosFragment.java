package org.luisma.lugarfinder;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.widget.AdapterView.OnItemClickListener;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.luisma.lugarfinder.adapter.AlojamientosAdapter;
import org.luisma.lugarfinder.lugares.Alojamiento;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class AlojamientosFragment extends Fragment implements OnItemClickListener{

    private ArrayList<Alojamiento> alojamientos;
    private AlojamientosAdapter alojamientosAdapter;

    public static String URL = "http://www.zaragoza.es/georref/json/hilo/ver_Alojamiento";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.alojamientos, container, false);

        alojamientos = new ArrayList<Alojamiento>();

        ListView listViewAlojamientos = (ListView) view.findViewById(R.id.lista_Alojamientos);
        alojamientosAdapter = new AlojamientosAdapter(getActivity(), alojamientos);
        listViewAlojamientos.setAdapter(alojamientosAdapter);
        listViewAlojamientos.setOnItemClickListener(this);
        cargarAlojamientos();
        return view;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Toast.makeText(getActivity(), "SE METE KO MAÑO", Toast.LENGTH_SHORT).show();
        if (i == ListView.INVALID_POSITION)
            return;

        Alojamiento alojamiento = alojamientos.get(i);

        Intent intent = new Intent(getActivity(), Mapa.class);
        intent.putExtra("latitud", alojamiento.getLatitud());
        intent.putExtra("longitud", alojamiento.getLongitud());
        intent.putExtra("nombre", alojamiento.getNombre());
        startActivity(intent);
    }


    private class TareaDescargaDatos extends AsyncTask<String, Void, Void> {

        private boolean error = false;

        @Override
        protected Void doInBackground(String... urls) {

            InputStream is = null;
            String resultado = null;
            JSONObject json = null;
            JSONArray jsonArray = null;
            alojamientos.clear();
            try {
                HttpClient clienteHttp = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost(urls[0]);
                HttpResponse respuesta = clienteHttp.execute(httpPost);
                HttpEntity entity = respuesta.getEntity();
                is = entity.getContent();

                BufferedReader br = new BufferedReader(new InputStreamReader(is));
                StringBuilder sb = new StringBuilder();
                String linea = null;

                while ((linea = br.readLine()) != null)
                    sb.append(linea + "\n");

                is.close();
                resultado = sb.toString();

                json = new JSONObject(resultado);
                jsonArray = json.getJSONArray("features");

                String nombre = null;
                String direccion = null;
                String categoria = null;
                String coordenadas = null;
                Alojamiento alojamiento = null;
                for (int i = 0; i < jsonArray.length(); i++) {

                    nombre = jsonArray.getJSONObject(i).getJSONObject("properties").getString("title");
                    direccion = jsonArray.getJSONObject(i).getJSONObject("properties").getString("description");
                    categoria = jsonArray.getJSONObject(i).getJSONObject("properties").getString("category");
                    coordenadas = jsonArray.getJSONObject(i).getJSONObject("geometry").getString("coordinates");
                    coordenadas = coordenadas.substring(1, coordenadas.length() - 1);
                    String latlong[] = coordenadas.split(",");
                    alojamiento = new Alojamiento(nombre, direccion, categoria,Float.parseFloat(latlong[0]),
                            Float.parseFloat(latlong[1]));
                    alojamientos.add(alojamiento);
                }

            } catch (ClientProtocolException cpe) {
                cpe.printStackTrace();
                error = true;
            } catch (IOException ioe) {
                ioe.printStackTrace();
                error = true;
            } catch (JSONException jse) {
                jse.printStackTrace();
                error = true;
            }

            return null;
        }


        @Override
        protected void onCancelled() {
            super.onCancelled();
            alojamientos.clear();
        }

        @Override
        protected void onProgressUpdate(Void... progreso) {
            super.onProgressUpdate(progreso);

            alojamientosAdapter.notifyDataSetChanged();
        }

        @Override
        protected void onPostExecute(Void resultado) {
            super.onPostExecute(resultado);

            if (error) {
                Toast.makeText(getActivity(), getResources().getString(R.string.error), Toast.LENGTH_SHORT).show();
                return;
            }

            alojamientosAdapter.notifyDataSetChanged();
            if(isAdded()){
                Toast.makeText(getActivity(), getResources().getString(R.string.datosdescargados), Toast.LENGTH_SHORT).show();
            }

        }
    }

    private void cargarAlojamientos() {
        TareaDescargaDatos tarea = new TareaDescargaDatos();
        tarea.execute(URL);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onResume() {
        super.onResume();
        alojamientosAdapter.notifyDataSetChanged();
        TareaDescargaDatos tarea = new TareaDescargaDatos();
        tarea.execute(URL);
    }
}