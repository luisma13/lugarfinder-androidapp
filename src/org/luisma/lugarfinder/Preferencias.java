package org.luisma.lugarfinder;

import android.os.Bundle;
import android.preference.PreferenceActivity;
import net.margaritov.preference.colorpicker.ColorPickerPreference;

public class Preferencias extends PreferenceActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addPreferencesFromResource(R.xml.preferencias);

    }

}
