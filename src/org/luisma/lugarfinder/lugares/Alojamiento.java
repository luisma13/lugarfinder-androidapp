package org.luisma.lugarfinder.lugares;

public class Alojamiento {
    private String nombre;
    private String direccion;
    private String categoria;
    private float latitud;
    private float longitud;

    public Alojamiento(String nombre, String direccion, String categoria, float latitud, float longitud) {
        this.nombre = nombre;
        this.direccion = direccion;
        this.categoria = categoria;
        this.latitud = latitud;
        this.longitud = longitud;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String descripcion) {
        this.direccion = descripcion;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public float getLatitud() {
        return latitud;
    }

    public void setLatitud(float latitud) {
        this.latitud = latitud;
    }

    public float getLongitud() {
        return longitud;
    }

    public void setLongitud(float longitud) {
        this.longitud = longitud;
    }

}
