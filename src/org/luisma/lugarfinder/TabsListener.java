package org.luisma.lugarfinder;

import android.app.ActionBar;
import android.app.Fragment;
import android.app.FragmentTransaction;

public class TabsListener implements ActionBar.TabListener {
    private Fragment fragment;

    public TabsListener(Fragment fragment){
        this.fragment = fragment;
    }

    //Se ha seleccionado una pestaña. Se carga en la pantalla

    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
        fragmentTransaction.replace(R.id.container, fragment);
    }

    //Se ha deseleccionado una pestaña. Se elimina de la pantalla

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
        fragmentTransaction.remove(fragment);
    }

    //Cuando pulsas de nuevo la pestaña

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
        fragment.onResume();
    }
}
