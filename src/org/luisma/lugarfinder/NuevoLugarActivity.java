package org.luisma.lugarfinder;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.media.MediaPlayer;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.*;
import org.luisma.lugarfinder.database.Database;
import org.luisma.lugarfinder.lugares.Lugar;

public class NuevoLugarActivity extends Activity implements View.OnClickListener{
    private final int RESULTADO_CARGA_IMAGEN = 1;
    private String accion;
    private int posicion;
    private Database db;
    private int id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.nuevo_lugar);
        db = new Database(this);

        Bundle bundle = getIntent().getExtras();
        accion = bundle.getString("accion");
        if (accion.equals("editar")) {
            posicion = bundle.getInt("posicion");
            cargarLugar(posicion);
        }
        Button btAceptar = (Button) findViewById(R.id.btAceptar);
        btAceptar.setOnClickListener(this);
        Button btCancelar = (Button) findViewById(R.id.btCancelar);
        btCancelar.setOnClickListener(this);
        Button btImagen = (Button) findViewById(R.id.btImagen);
        btImagen.setOnClickListener(this);
    }

    public void cargarLugar(int i){
        Lugar lugar = LugaresFragment.lugares.get(i);
        id = db.conocerIDLugar(lugar);
        ((EditText) findViewById(R.id.tvNombre)).setText(lugar.getNombre());

        RadioButton rbRestaurante = (RadioButton) findViewById(R.id.rbRestaurante);
        RadioButton rbAlojamiento = (RadioButton) findViewById(R.id.rbAlojamiento);
        RadioButton rbBar = (RadioButton) findViewById(R.id.rbBar);
        RadioButton rbTienda = (RadioButton) findViewById(R.id.rbTienda);
        RadioButton rbCentroComercial = (RadioButton) findViewById(R.id.rbCentroComercial);
        RadioButton rbVivienda = (RadioButton) findViewById(R.id.rbVivienda);

        if (lugar.getTipo() != null) {
            if (lugar.getTipo().equals(rbRestaurante.getText().toString()))
                rbRestaurante.setChecked(true);
            else if (lugar.getTipo().equals(rbAlojamiento.getText().toString()))
                rbAlojamiento.setChecked(true);
            else if (lugar.getTipo().equals(rbBar.getText().toString()))
                rbBar.setChecked(true);
            else if (lugar.getTipo().equals(rbTienda.getText().toString()))
                rbTienda.setChecked(true);
            else if (lugar.getTipo().equals(rbCentroComercial.getText().toString()))
                rbCentroComercial.setChecked(true);
            else if (lugar.getTipo().equals(rbVivienda.getText().toString()))
                rbVivienda.setChecked(true);
        }
        ((EditText) findViewById(R.id.tvDescripcion)).setText(lugar.getDescripcion());
        ((EditText) findViewById(R.id.tvDireccion)).setText(lugar.getDireccion());
        ((EditText) findViewById(R.id.tvOpinion)).setText(String.valueOf(lugar.getOpinion()));
        ((ImageView) findViewById(R.id.imagen)).setImageBitmap(lugar.getImagen());
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btAceptar:
                EditText nombre = (EditText) findViewById(R.id.tvNombre);

                RadioButton rbRestaurante = (RadioButton) findViewById(R.id.rbRestaurante);
                RadioButton rbAlojamiento = (RadioButton) findViewById(R.id.rbAlojamiento);
                RadioButton rbBar = (RadioButton) findViewById(R.id.rbBar);
                RadioButton rbTienda = (RadioButton) findViewById(R.id.rbTienda);
                RadioButton rbCentroComercial = (RadioButton) findViewById(R.id.rbCentroComercial);
                RadioButton rbVivienda = (RadioButton) findViewById(R.id.rbVivienda);

                EditText descripcion = (EditText) findViewById(R.id.tvDescripcion);
                EditText direccion = (EditText) findViewById(R.id.tvDireccion);
                EditText opinion = (EditText) findViewById(R.id.tvOpinion);

                ImageView imagen = (ImageView) findViewById(R.id.imagen);

                Lugar lugar = null;

                if (accion.equals("nuevo"))
                    lugar = new Lugar();
                else
                    lugar = LugaresFragment.lugares.get(posicion);

                if (!nombre.getText().toString().equals(""))
                    lugar.setNombre(nombre.getText().toString());

                if (rbRestaurante.isChecked())
                    lugar.setTipo(rbRestaurante.getText().toString());
                else if (rbAlojamiento.isChecked())
                    lugar.setTipo(rbAlojamiento.getText().toString());
                else if (rbBar.isChecked())
                    lugar.setTipo(rbBar.getText().toString());
                else if (rbTienda.isChecked())
                    lugar.setTipo(rbTienda.getText().toString());
                else if (rbCentroComercial.isChecked())
                    lugar.setTipo(rbCentroComercial.getText().toString());
                else if (rbVivienda.isChecked())
                    lugar.setTipo(rbVivienda.getText().toString());

                if (!descripcion.getText().toString().equals(""))
                    lugar.setDescripcion(descripcion.getText().toString());
                if (!direccion.getText().toString().equals(""))
                    lugar.setDireccion(direccion.getText().toString());
                if (!opinion.getText().toString().equals("")) {
                    if (Integer.parseInt(opinion.getText().toString()) > 10)
                        lugar.setOpinion(10);
                    else
                        lugar.setOpinion(Integer.parseInt(opinion.getText().toString()));
                }
                lugar.setImagen(((BitmapDrawable) imagen.getDrawable()).getBitmap());
                if (lugar.getNombre() == null) {
                    Toast.makeText(this, getResources().getString(R.string.nombreobligatorio), Toast.LENGTH_LONG).show();
                    break;
                }
                if (lugar.getDescripcion() == null) {
                    Toast.makeText(this, getResources().getString(R.string.descripcionobligatoria), Toast.LENGTH_LONG).show();
                    break;
                }
                if (lugar.getDireccion() == null){
                    Toast.makeText(this, getResources().getString(R.string.direccionobligatoria), Toast.LENGTH_LONG).show();
                    break;
                }

                if (accion.equals("nuevo"))
                    db.nuevoLugar(lugar);
                else if (accion.equals("editar"))
                    db.modificarLugar(lugar, id);
                Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                MediaPlayer mediaPlayer = MediaPlayer.create(getBaseContext(), uri);
                mediaPlayer.setLooping(false);
                mediaPlayer.start();

                finish();

                break;
            case R.id.btCancelar:
                finish();
                break;
            case R.id.btImagen:
                Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(intent, RESULTADO_CARGA_IMAGEN);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if ((requestCode == RESULTADO_CARGA_IMAGEN)&&(data != null)){
            Uri imagenSeleccionada = data.getData();
            String[] ruta = {MediaStore.Images.Media.DATA};

            Cursor cursor = getContentResolver().query(imagenSeleccionada, ruta, null, null, null);
            cursor.moveToFirst();

            int indice = cursor.getColumnIndex(ruta[0]);
            String picturePath = cursor.getString(indice);
            cursor.close();

            ImageView imagen = (ImageView) findViewById(R.id.imagen);

            imagen.setImageBitmap(BitmapFactory.decodeFile(picturePath));

        }
    }
}
