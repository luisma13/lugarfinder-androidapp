package org.luisma.lugarfinder.database;

import android.provider.BaseColumns;

public interface Constants extends BaseColumns {
    public static final String TABLA_LUGARES = "lugares";

    public static final String NOMBRE = "nombre";
    public static final String TIPO_LUGAR = "tipo";
    public static final String DESCRIPCION = "descripcion";
    public static final String DIRECCION = "direccion";
    public static final String OPINION = "opinion";
    public static final String RUTAIMAGEN = "rutaimagen";
}
