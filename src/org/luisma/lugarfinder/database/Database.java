package org.luisma.lugarfinder.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import org.luisma.lugarfinder.lugares.Lugar;

import java.io.ByteArrayOutputStream;

import static android.provider.BaseColumns._ID;
import static org.luisma.lugarfinder.database.Constants.*;

public class Database extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "lugarfinder.db";
    private static final int DATABASE_VERSION = 1;

    private static String[] COLUMNAS = {_ID, NOMBRE, TIPO_LUGAR, DESCRIPCION, DIRECCION, OPINION, RUTAIMAGEN};
    private static String ORDER_BY = NOMBRE + " DESC";

    public Database(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + TABLA_LUGARES + "("
                + _ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + NOMBRE + " TEXT, " + TIPO_LUGAR +" TEXT, "+
                DESCRIPCION + " TEXT, " + DIRECCION + " TEXT, " +
                OPINION + " INTEGER, "+ RUTAIMAGEN + " BLOB);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int previousVersion, int nextVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLA_LUGARES);
        onCreate(db);
    }

    public void nuevoLugar(Lugar lugar) {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(NOMBRE, lugar.getNombre());
        values.put(TIPO_LUGAR, lugar.getTipo());
        values.put(DESCRIPCION, lugar.getDescripcion());
        values.put(DIRECCION, lugar.getDireccion());
        values.put(OPINION, lugar.getOpinion());

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        lugar.getImagen().compress(Bitmap.CompressFormat.JPEG, 100, stream);
        byte[] byteArray = stream.toByteArray();
        values.put(RUTAIMAGEN, byteArray);

        db.insertOrThrow(TABLA_LUGARES, null, values);
    }

    public void modificarLugar(Lugar lugar, int id) {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(NOMBRE, lugar.getNombre());
        values.put(TIPO_LUGAR, lugar.getTipo());
        values.put(DESCRIPCION, lugar.getDescripcion());
        values.put(DIRECCION, lugar.getDireccion());
        values.put(OPINION, lugar.getOpinion());
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        lugar.getImagen().compress(Bitmap.CompressFormat.JPEG, 100, stream);
        byte[] byteArray = stream.toByteArray();
        values.put(RUTAIMAGEN, byteArray);

        db.update(TABLA_LUGARES, values, _ID + "=" + id, null);
    }

    public int conocerIDLugar(Lugar lugar){
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery("SELECT "+ _ID +" FROM " + TABLA_LUGARES + " WHERE " +
                NOMBRE + "= ? and "  + DESCRIPCION + "= ? and " + DIRECCION + "= ?"
                ,new String[]{lugar.getNombre(), lugar.getDescripcion(), lugar.getDireccion()});

        cursor.moveToFirst();
        int id = cursor.getInt(0);
        return id;
    }

    public void eliminarLugar(int id){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLA_LUGARES, _ID + "=" + id, null);
    }

    public Cursor getLugares() {

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLA_LUGARES, COLUMNAS, null, null, null, null, ORDER_BY);

        return cursor;
    }
}
