package org.luisma.lugarfinder;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.os.Bundle;
import com.google.android.gms.maps.*;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import org.luisma.lugarfinder.lugares.Restaurante;
import org.luisma.lugarfinder.util.Util;

public class Mapa extends Activity{

    private GoogleMap mapa;

    private double latitud;
    private double longitud;
    private String nombre;
    private boolean todosRestaurantesCercanos;
    private CameraUpdate camara;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mapa);

        Intent i = getIntent();

        todosRestaurantesCercanos = i.getBooleanExtra("todos", false);

        MapsInitializer.initialize(this);

        if (todosRestaurantesCercanos) {
            mapa = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
            mapa.clear();
            ubicarme();
        }else{
            latitud = i.getFloatExtra("latitud", 0);
            longitud = i.getFloatExtra("longitud", 0);
            nombre = i.getStringExtra("nombre");
            uk.me.jstott.jcoord.LatLng ubicacion = Util.DeUMTSaLatLng(latitud, longitud, 'N', 30);
            this.latitud = ubicacion.getLat();
            this.longitud = ubicacion.getLng();
            mapa = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
            mapa.clear();
            ubicarRestaurante();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void ubicarme(){
        BitmapDescriptor icon = BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN);
        MarkerOptions centroZgz = new MarkerOptions()
                .position(new LatLng(41.64879080, -0.88958109))
                .title("Centro de Zaragoza")
                .icon(icon);
        mapa.addMarker(centroZgz);
        for (Restaurante restaurante : MyActivity.RESTAURANTES){
            uk.me.jstott.jcoord.LatLng latLng = Util.DeUMTSaLatLng(restaurante.getLatitud(), restaurante.getLongitud(), 'N', 30);
            mapa.addMarker(new MarkerOptions()
                .position(new LatLng(latLng.getLat(), latLng.getLng()))
                .title(restaurante.getNombre()));
        }
        camara =
                CameraUpdateFactory.newLatLng(centroZgz.getPosition());
        mapa.moveCamera(camara);
        mapa.animateCamera(CameraUpdateFactory.zoomTo(17.0f));
    }

    private void ubicarRestaurante() {
        camara =
                CameraUpdateFactory.newLatLng(new LatLng(latitud, longitud));

        mapa.moveCamera(camara);
        mapa.animateCamera(CameraUpdateFactory.zoomTo(17.0f));
        mapa.addMarker(new MarkerOptions()
                .position(new LatLng(latitud, longitud))
                .title(nombre));
    }
}

